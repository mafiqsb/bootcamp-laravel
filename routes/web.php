<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\Usercontroller;
use App\Http\Controllers\Hellocontroller;
use App\Http\Controllers\MemberController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
}); 


Route::get('/member/login', [ MemberController::class, 'login'] );

Route::get('/member/register', [ MemberController::class, 'register'] );

Route::get('/member', [ MemberController::class, 'index'] );

Route::get('/member/profile', [ MemberController::class, 'profile'] );

Route::view('dashboard', 'dashboard')
    ->name('dashboard')
    ->middleware(['auth', 'verified']);

// Route::get('/kelas', function(){
//     return view ("kelas");
// });



// Route::get('/hello/{nama}', [ Hellocontroller::class, 'index']);

// Route::get('users', [Usercontroller::class, 'index']);

// Route::get('books', [BookController::class, 'index']);

// //form untuk create new book

// Route::get('book/new', [BookController::class, 'add']);

// // memproses new book creation


// Route::post('book/new', [BookController::class, 'store']);

// //edit
Route::view('dashboard', 'dashboard')
	->name('dashboard')
	->middleware(['auth', 'verified']);
