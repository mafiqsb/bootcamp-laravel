<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Senarai Buku</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h1>Senarai Buku</h1>
                <hr>
                <a href="/book/new" class="btn btn-primary btn-sm">Buku Baru</a>


                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>JUDUL</th>
                            <th>PENULIS</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($books as $book)
                        <tr>
                            <td>{{$book['id']}}</td>
                            <td>{{$book['title']}}</td>
                            <td>{{$book->user->name}}</td>
                            <td><button class="btn btn-primary btn-sm">EDIT</button></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{$books->links()}}
            </div>
        </div>
    </div>
</body>
</html>