<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buku baru</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <a href="/books" class="btn btn-primary btn-sm">&laquo; Kembali ke Senarai Buku</a>
                <h1>Buku Baru</h1>
                <hr>
                <form action="/book/new" method="post">

                @csrf

                    <label for="" class="mt-3">Judul</label>
                    <input type="text" class="form-control mb-2 @error('title') is-invalid @enderror" name="title" value="{{old('title')}}">
                    <div class="invalid-feedback">@error('title') {{$message}} @enderror</div>

                    <label for="" class="mt-3">Ringkasan</label>
                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" row="4">{{old('description')}}</textarea>
                    <div class="invalid-feedback">@error('description') {{$message}} @enderror</div>
                    
                    <label for="" class="mt-3">Penulis</label>
                    <select name="user_id" class="form-control">
                        @foreach ($users as $user)
                        <option value="{{ $user['id'] }}"> {{ $user['name']}} </option>
                        @endforeach
                    </select>

                    <button type="submit" class="mt-3 btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</body>

</html>