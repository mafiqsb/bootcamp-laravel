@extends('layout.public')

@section('content')
@if (session('status'))
<div>
    {{ session('status') }}
</div>
@endif

@if ($errors->any())
<div>
    <div>{{ __('Whoops! Something went wrong.') }}</div>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form class="user" method="POST" action="{{ route('login') }}">
    @csrf

    <div class="form-group">
        <label>{{ __('Email') }}</label>
        <input class="form-control form-control-user" type="email" name="email" value="{{ old('email') }}" placeholder="Enter Email Address" required
            autofocus />
    </div>

    <div class="form-group">
        <label>{{ __('Password') }}</label>
        <input class="form-control form-control-user" type="password" name="password" required
            autocomplete="current-password" aria-describedby="emailHelp" placeholder="Enter Password" />
    </div>

    <!-- <div>
            <label>{{ __('Remember me') }}</label>
            <input type="checkbox" name="remember" class="form-control form-control-user">
        </div> -->
    

    <div class="form-group">
        <div class="custom-control custom-checkbox small">
            <input type="checkbox" class="custom-control-input" id="customCheck">
            <label class="custom-control-label" for="customCheck">Remember
                Me</label>
        </div>
    </div>

    <div>
        <button type="submit" class="btn btn-primary btn-user">
            {{ __('Login') }}
        </button>
    </div>

    <hr>
    

    @if (Route::has('password.request'))


    <div class="text-center">
        <a class="small" href="{{ route('password.request') }}">
            {{ __('Forgot your password?') }}
        </a>
    </div>

    @endif

    <div class="text-center">
        <a class="small" href="forgot-password.html">Create an Account</a>
    </div>


</form>
@endsection
