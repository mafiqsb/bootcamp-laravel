<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Hellocontroller extends Controller
{
    public function index($nama) {
        $data = [
            'nama' => $nama
        ];

        return view('hello', $data);
    }
}
