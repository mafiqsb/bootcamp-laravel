<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class Usercontroller extends Controller
{
    public function index() {
        $users = User::all();

        $data = [
            'users' => $users
        ];
        return view('user-list', $data);
    }
}
