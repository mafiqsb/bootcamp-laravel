<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index(){
        $books = Book::with('user')->paginate(5);

        return view('books.index', compact('books'));
    }

    public function add() {

        $users = User::all();
        $data = [
            'users' =>$users
        ];
        return view('books.add-form', $data);
    }

    public function store(Request $request) {
        //dd($request);
        $validated_data = $request->validate([
            'title' => 'required |min:5|max:255',
            'description' => 'required|min:10|max:1000',
            'user_id' => 'required'
        ]);
        
        $book = Book::Create($validated_data);
        //([
        //     'title' => $request -> title,
        //     'description' => $request -> description,
        //     'user_id' => $request -> user_id
        // ]);


        return redirect('books');
    }
}
